import java.util.Scanner;
public class Roulette{
    public static void main(String[]args){
        Scanner scan = new Scanner(System.in);
        RouletteWheel Rw = new RouletteWheel();
        System.out.println("Welcome to spin the wheel, you start with 1000$");
        boolean over = false;
        boolean answerBool;
        int playerCash = 1000;
        int betAmount;
        int playerNum;
        char answer;
        char tryagain;
        while (!over){

            System.out.println("You currently have, "+playerCash+" $" );
            System.out.println("Do you want to bet? y or n");
            answer = scan.next().charAt(0);

            while (answer != 'y' && answer != 'n'){
                System.out.println("Do you want to bet? y or n");
                answer = scan.next().charAt(0);
            }

            if (answer == 'y'){
               System.out.println("How much are you willing to bet?");
                betAmount = scan.nextInt();
                while (betAmount>playerCash){
                    System.out.println("You cant bet this amount!");
                    betAmount = scan.nextInt();
                }
                System.out.println("Choose a number between 0 and 36");
                playerNum = scan.nextInt();
                while (playerNum >36 || playerNum<0){
                    System.out.println("Choose a number between 0 and 36");
                    playerNum = scan.nextInt();
                }

                Rw.spin();
                answerBool = checkNum(betAmount,Rw.getValue());
                if (answerBool){
                    System.out.println("Congrats you win "+betAmount*37+" $!!");
                    playerCash = playerCash-betAmount+betAmount*35;
                    System.out.println("New total is "+playerCash+" $!!");
                }
                else{
                    System.out.println("You lose...");
                    playerCash = playerCash-betAmount;
                    System.out.println("New total is "+playerCash+" $");
                }
                System.out.println("Try again? y or n");
                tryagain = scan.next().charAt(0);

            while (tryagain != 'y' && tryagain != 'n'){
                System.out.println("Do you want to try again? y or n");
                tryagain = scan.next().charAt(0);
            }
            if(tryagain == 'y'){
                over = false;
            }
            else{
                System.out.println("Game over!");
                over=true;
            }
            }
        else{
             System.out.println("Game over!");
            over=true;
        }
           
        }
        
}
public static boolean checkNum(int playerNum, int spinNum){
        if (playerNum==spinNum){
            return true;
        }
        else {
            return false;
        }
    } 
    
}